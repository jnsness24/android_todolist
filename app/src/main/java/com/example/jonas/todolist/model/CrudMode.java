package com.example.jonas.todolist.model;

public enum CrudMode {
    SIMPLE, LOCAL, REMOTE;
}
