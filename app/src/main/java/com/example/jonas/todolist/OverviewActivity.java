package com.example.jonas.todolist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.jonas.todolist.model.DataItem;
import com.example.jonas.todolist.model.IDataItemCRUDOperations;
import com.example.jonas.todolist.model.IDataItemCRUDOperationsAsync;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OverviewActivity extends AppCompatActivity {

    public static final Integer CALL_EDIT_ITEM = 0;
    public static final Integer CALL_CREATE_ITEM = 1;
    public static final int CALL_PICK_CONTACT = 2;
    public static final int REQUEST_PERMISSIONS = 4;

    private IDataItemCRUDOperationsAsync crudOperations;

    private ViewGroup listView;
    private ArrayAdapter<DataItem> listViewAdapter;
    private FloatingActionButton addButton;

    private SortMode activeSortMode;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);

        this.crudOperations = ((DataItemApplication)getApplication()).getCRUDOperations();
        if (!((DataItemApplication) getApplication()).getConnection()) {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Connection to Server - changed to local operations", Snackbar.LENGTH_LONG);
            snackbar.show();
//            Toast.makeText(this, "No Connection to Server - changed to local operations", Toast.LENGTH_SHORT).show();
        }
        

        listView = findViewById(R.id.list_view_id);
        addButton = findViewById(R.id.button_add_id);

        this.activeSortMode = SortMode.SORT_BY_RELEVANCE;

        listViewAdapter = new ArrayAdapter<DataItem>(this, R.layout.activity_overview_listitem){
            @NonNull
            @Override
            public View getView(int position, @Nullable View existingView, @NonNull ViewGroup parent) {

                final DataItem item = this.getItem(position);

                View itemView = existingView;

                if (itemView == null) {
                    Log.i("OverviewActivity", "create new View for position " + position);
                    itemView = getLayoutInflater().inflate(R.layout.activity_overview_listitem, null);
                } else {
                    Log.i("OverviewActivity", "recycling existing view for position " + position);
                }
                TextView itemNameText = itemView.findViewById(R.id.textview_itemname);
                itemNameText.setText(item.getName());

                TextView itemId = itemView.findViewById(R.id.textview_item_id);
                itemId.setText(String.valueOf(item.getId()));

                TextView itemExpiry = itemView.findViewById(R.id.textview_expiry);
                Calendar todayCalendar = Calendar.getInstance();

                if (item.getExpiry() < todayCalendar.getTimeInMillis()) {
                    itemExpiry.setText(item.getDateFormatted());
                    itemExpiry.setTextColor(Color.RED);
                }
                else {
                    itemExpiry.setText(item.getDateFormatted());
                }


                ToggleButton itemFavourite = itemView.findViewById(R.id.togglebutton_favourite);

                // need to "null" the ChangeListener because of recycling of listView Elements
                itemFavourite.setOnCheckedChangeListener(null);
                itemFavourite.setChecked(item.isFavourite());


                itemFavourite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        item.setFavourite(isChecked);
                        crudOperations.updateItem(item.getId(), item, new IDataItemCRUDOperationsAsync.ResultCallback<Boolean>() {
                            @Override
                            public void onresult(Boolean result) {
                                Toast.makeText(OverviewActivity.this, "Favourite Toggle Activated with ID " + item.getId(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

                CheckBox itemDoneCheckbox = itemView.findViewById(R.id.itemDone);

                // need to "null" the ChangeListener because of recycling of listView Elements
                itemDoneCheckbox.setOnCheckedChangeListener(null);
                itemDoneCheckbox.setChecked(item.isDone());

                itemDoneCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        item.setDone(isChecked);
                        crudOperations.updateItem(item.getId(), item, new IDataItemCRUDOperationsAsync.ResultCallback<Boolean>() {
                            @Override
                            public void onresult(Boolean result) {
                                Toast.makeText(OverviewActivity.this, "Item with id" + item.getId() + " has been updated", Toast.LENGTH_LONG).show();
                                sortItems();
                            }
                        });
                    }
                });

                return itemView;
            }
        };

        listViewAdapter.setNotifyOnChange(true);
        ((ListView) listView).setAdapter(listViewAdapter);

        reloadItemList();




        ((ListView) listView).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DataItem selectedItem = listViewAdapter.getItem(position);
                showDetailviewForEdit(selectedItem);
            }
        });


        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailviewForCreate();
            }
        });
    }

    private void reloadItemList() {
        crudOperations.readAllItems(new IDataItemCRUDOperationsAsync.ResultCallback<List<DataItem>>() {
            @Override
            public void onresult(List<DataItem> result) {
                listViewAdapter.addAll(result);
            }
        });
    }

    private void addItemToList(DataItem item) {
        this.listViewAdapter.add(item);
        ((ListView)this.listView).setSelection(this.listViewAdapter.getPosition(item));
    }

    private void updateItemInList(DataItem item) {
        //TODO clean up dirty implementation - currently everything is reloaded after an update is done
        ((ListView)this.listView).setSelection(this.listViewAdapter.getPosition(item));
        listViewAdapter.clear();
        reloadItemList();


    }

    private void showDetailviewForCreate() {
        Intent callDetailviewForCreateIntent = new Intent(this, DetailviewActivity.class);
        startActivityForResult(callDetailviewForCreateIntent, CALL_CREATE_ITEM);

    }

    private void showDetailviewForEdit(DataItem item) {
        Intent callDetailviewIntent = new Intent(this, DetailviewActivity.class);
        callDetailviewIntent.putExtra(DetailviewActivity.ARG_ITEM_ID, item.getId());
        startActivityForResult(callDetailviewIntent, CALL_EDIT_ITEM);
    }



    // Handles every Result, depending on Request and Result Code of the received intent Object (put Extra)
    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent intent) {
        if (requestCode == CALL_EDIT_ITEM || requestCode == CALL_CREATE_ITEM) {
            if(resultCode == RESULT_OK) {
                long itemId = intent.getLongExtra(DetailviewActivity.ARG_ITEM_ID, -1);
                crudOperations.readItem(itemId, new IDataItemCRUDOperationsAsync.ResultCallback<DataItem>() {
                    @Override
                    public void onresult(DataItem result) {
                        if (requestCode == CALL_EDIT_ITEM) {
                            updateItemInList(result);
                        }
                        else
                        {
                            addItemToList(result);
                        }
                    }
                });

            }
            else {
                Toast.makeText(OverviewActivity.this, "no itemName received from detailView", Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_overview, menu);
        return true;
    }

    /* sort items functionality*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()  == R.id.sortItemsDate) {
            this.activeSortMode = SortMode.SORT_BY_DATE;
            this.sortItems();
            return true;
        }
        else {
            if (item.getItemId() == R.id.sortItemsRelevance) {
                this.activeSortMode = SortMode.SORT_BY_RELEVANCE;
                this.sortItems();
                return true;
            }
        }
        return false;
    }

    private void sortItems() {
        switch(activeSortMode) {
            case SORT_BY_ID:
                listViewAdapter.sort(DataItem.SORT_BY_ID);
                break;
            case SORT_BY_NAME:
                listViewAdapter.sort(DataItem.SORT_BY_NAME);
                break;
            case SORT_BY_DONE:
                listViewAdapter.sort(DataItem.SORT_BY_DONE);
                break;
            case SORT_BY_RELEVANCE:
                listViewAdapter.sort(DataItem.SORT_BY_DATE);
                listViewAdapter.sort(DataItem.SORT_BY_RELEVANCE);
                listViewAdapter.sort(DataItem.SORT_BY_DONE);
                break;
            case SORT_BY_DATE:
                listViewAdapter.sort(DataItem.SORT_BY_RELEVANCE);
                listViewAdapter.sort(DataItem.SORT_BY_DATE);
                listViewAdapter.sort(DataItem.SORT_BY_DONE);

                break;
        }
    }


}
