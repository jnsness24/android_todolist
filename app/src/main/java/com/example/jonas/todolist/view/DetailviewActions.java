package com.example.jonas.todolist.view;

// Interface for DataBinding
public interface DetailviewActions {

        public void saveItem();

        public void showDatePicker();

        public void deleteItem();

        public void toggleDone();

        public void toggleFavourite();

}
