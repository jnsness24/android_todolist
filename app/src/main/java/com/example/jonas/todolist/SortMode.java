package com.example.jonas.todolist;

public enum SortMode {
        SORT_BY_ID, SORT_BY_NAME, SORT_BY_DONE, SORT_BY_DATE, SORT_BY_RELEVANCE;
}
