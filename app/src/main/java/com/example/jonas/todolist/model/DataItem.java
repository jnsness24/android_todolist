package com.example.jonas.todolist.model;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class DataItem implements Serializable{

    private long id = -1;
    private String name;
    private String description;
    private long expiry;



    private boolean favourite;
    private boolean done;
    private ArrayList<String> contacts = new ArrayList<>();





    public DataItem(String name) {
        this.name = name;
        this.expiry = expiry;
    }

    public DataItem() {

    }

    public long getId() {
        return id;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDateFormatted() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        return formatter.format(new Date(this.expiry));
    }

    public void addContact(Contact newContact) {
        this.contacts.add(newContact.getId());
        Log.i("Debug", "addContact: " + contacts);
        }


    public ArrayList<String> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<String> contacts) {
        this.contacts = contacts;
    }

    public void deleteContact(String id) {
        this.contacts.remove(id);
        }

    public static Comparator<DataItem> SORT_BY_NAME = new Comparator<DataItem>() {
        @Override
        public int compare(DataItem obj1, DataItem obj2) {
            return String.valueOf(obj1.getName()).toLowerCase().compareTo(String.valueOf(obj2.getName()).toLowerCase());
        }
    };

    public static Comparator<DataItem> SORT_BY_ID = new Comparator<DataItem>() {
        @Override
        public int compare(DataItem obj1, DataItem obj2) {
            return (int) (obj1.getId() - obj2.getId());
        }
    };


    public static Comparator<DataItem> SORT_BY_DATE = new Comparator<DataItem>() {
        @Override
        public int compare(DataItem obj1, DataItem obj2) {
            Log.i("Debug", "compare: obj1 - " +obj1.getExpiry() + " formatted " + obj1.getDateFormatted() + " MINUS " + obj2.getExpiry() + " formatted " + obj2.getDateFormatted());
            Log.i("Debug", "sum " + (int) (obj1.getExpiry()-obj2.getExpiry()));
            return obj1.getDateFormatted().compareTo(obj2.getDateFormatted());


        }
    };


    public static Comparator<DataItem> SORT_BY_RELEVANCE = new Comparator<DataItem>() {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public int compare(DataItem obj1, DataItem obj2) {
            // -1 turns around the list - so that the favourite Items are above
            return (Boolean.compare(obj1.isFavourite(), obj2.isFavourite())*-1);
        }
    };


    public static Comparator<DataItem> SORT_BY_DONE = new Comparator<DataItem>() {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public int compare(DataItem obj1, DataItem obj2) {
            return Boolean.compare(obj1.isDone(), obj2.isDone());
        }
    };


}
