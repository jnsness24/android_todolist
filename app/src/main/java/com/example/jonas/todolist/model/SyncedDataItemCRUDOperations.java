package com.example.jonas.todolist.model;

import java.util.List;

public class SyncedDataItemCRUDOperations implements IDataItemCRUDOperations {

    private LocalDataitemCRUDOperations localCrud;
    private RemoteDataitemCRUDOperationsimpl remoteCrud;

    public SyncedDataItemCRUDOperations(LocalDataitemCRUDOperations localCrud, RemoteDataitemCRUDOperationsimpl remoteCrud) {
        this.localCrud = localCrud;
        this.remoteCrud = remoteCrud;
    }

    @Override
    public long createItem(DataItem item) {
        long id = localCrud.createItem(item);
        item.setId(id);
        remoteCrud.createItem(item);
        return id;
    }

    @Override
    public List<DataItem> readAllItems() {
        return localCrud.readAllItems();
    }

    @Override
    public DataItem readItem(long id) {
        return localCrud.readItem(id);
    }

    @Override
    public boolean updateItem(long id, DataItem item) {
        if (localCrud.updateItem(id, item)) {
            remoteCrud.updateItem(id, item);
        }

        return true;
    }

    @Override
    public boolean deleteItem(long id) {
        if (localCrud.deleteItem(id)) {
            remoteCrud.deleteItem(id);
        }

        return true;
    }

    public void doSync() {
        List<DataItem> localItems = localCrud.readAllItems();
        if (!localItems.isEmpty()) {
            syncFromLocalToRemote(localItems);
        } else {
            List<DataItem> remoteItems = remoteCrud.readAllItems();
            syncFromRemoteToLocal(remoteItems);
            //for synchronized ID we have to do things twice
            syncFromLocalToRemote(localItems);
        }

    }

    private void syncFromRemoteToLocal(List<DataItem> remoteItems) {
        for (DataItem item : remoteItems) {
            localCrud.createItem(item);
        }
    }

    private void syncFromLocalToRemote(List<DataItem> localItems) {
        remoteCrud.deleteAll();
        for (DataItem item: localItems) {
            remoteCrud.createItem(item);
        }
    }
}
