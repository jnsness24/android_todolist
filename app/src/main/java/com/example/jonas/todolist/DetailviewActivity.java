package com.example.jonas.todolist;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.jonas.todolist.databinding.ActivityDetailviewBinding;
import com.example.jonas.todolist.model.Contact;
import com.example.jonas.todolist.model.DataItem;
import com.example.jonas.todolist.model.IDataItemCRUDOperations;
import com.example.jonas.todolist.model.IDataItemCRUDOperationsAsync;
import com.example.jonas.todolist.view.DetailviewActions;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class DetailviewActivity extends AppCompatActivity implements DetailviewActions {

    private IDataItemCRUDOperationsAsync crudOperations;
    public static final String ARG_ITEM_ID = "itemId";
    private DataItem item;

    //    private Date date;
    private Calendar calendar;

    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailview);

        final ActivityDetailviewBinding bindingMediator = DataBindingUtil.setContentView(this, R.layout.activity_detailview);
        calendar = Calendar.getInstance();


        crudOperations = ((DataItemApplication) getApplication()).getCRUDOperations();

        long itemId = getIntent().getLongExtra(ARG_ITEM_ID, -1);
        if (itemId != -1) {
            //          this.item = crudOperations.readItem(itemId);

            crudOperations.readItem(itemId, new IDataItemCRUDOperationsAsync.ResultCallback<DataItem>() {
                @Override
                public void onresult(DataItem result) {
                    item = result;
                    setBindings(bindingMediator);

                    Log.i("Debug", "Item already exists; has the following Options | ID: " + result.getId() + " | Name: " + result.getName());
                    Log.i("contacts", "onCreate: Contacts:" + item.getContacts());
                    updateContactListLayout();
                }
            });


        } else {
            this.item = new DataItem();
            setBindings(bindingMediator);

            Log.i("Debug", "Item has no ID or does not exist, now generating new Item");
        }


        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                calendar.set(year, month, dayOfMonth);
                Log.i("Debug", "onDateSet: current date is " + calendar.toString());

                item.setExpiry(calendar.getTimeInMillis());
                setBindings(bindingMediator);
            }
        };

        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                setBindings(bindingMediator);
                Log.i("Debug", "onTimeSet: current date is " + calendar.toString());

            }
        };


    }

    private void setBindings(ActivityDetailviewBinding bindingMediator) {
        bindingMediator.setItem(item);
        bindingMediator.setActions(this);
    }

    public void showTimePicker() {
        TimePickerDialog dialog = new TimePickerDialog(this, R.style.Theme_AppCompat_Light_Dialog_MinWidth, mTimeSetListener, 24, 60, true);
        dialog.show();
    }

    public void showDatePicker() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, R.style.Theme_AppCompat_Light_Dialog_MinWidth,
                mDateSetListener, year, month, day);
        dialog.show();
        showTimePicker();

    }

    public void saveItem() {

        if (this.item.getId() == -1) {
            crudOperations.createItem(this.item, new IDataItemCRUDOperationsAsync.ResultCallback<Long>() {
                @Override
                public void onresult(Long result) {
                    item.setId(result);
                    returnToOverview();
                }
            });
        } else {
            crudOperations.updateItem(item.getId(), item, new IDataItemCRUDOperationsAsync.ResultCallback<Boolean>() {
                @Override
                public void onresult(Boolean result) {
                    returnToOverview();
                }
            });

        }
    }

    public void deleteItem() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Toast.makeText(DetailviewActivity.this, "This was a Yes", Toast.LENGTH_SHORT).show();
                        crudOperations.deleteItem(item.getId(), new IDataItemCRUDOperationsAsync.ResultCallback<Boolean>() {
                            @Override
                            public void onresult(Boolean result) {
                                returnToOverview();
                            }
                        });
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        Toast.makeText(DetailviewActivity.this, "No NO nO!!!", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Delete this item? Are you sure?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();

    }

    public void toggleFavourite() {
        if (this.item.isFavourite()) {
            this.item.setFavourite(false);
        } else {
            this.item.setFavourite(true);
        }
    }

    public void toggleDone() {
        if (this.item.isDone()) {
            this.item.setDone(false);
        } else {
            this.item.setDone(true);
        }
    }

    public void returnToOverview() {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(ARG_ITEM_ID, this.item.getId());

        setResult(RESULT_OK, returnIntent);
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detailview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.addContactId) {
            pickContacts();
            return true;
        } else {
            return false;
        }
    }

    private void pickContacts() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(pickContactIntent, OverviewActivity.CALL_PICK_CONTACT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OverviewActivity.CALL_PICK_CONTACT) {
            if (resultCode == RESULT_OK) {
                selectContact(data.getData());
            } else {
                Toast.makeText(this, "Contact Pick aborted", Toast.LENGTH_SHORT).show();
            }
        }
    }

//TODO Bug in here - in Intent, when you go back with backbutton, App crashes
    @TargetApi(Build.VERSION_CODES.M)
    private void selectContact(Uri contactUri) {
        Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);
        if (cursor.moveToFirst()) {

            String currentNumber = null;
            String currenteMailAddress = null;
            String currentName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            String currentId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

            // Permissions on the fly
            int hasReadContactsPermissions = checkSelfPermission(Manifest.permission.READ_CONTACTS);
            if (hasReadContactsPermissions == PackageManager.PERMISSION_GRANTED) {
                Log.i("Debug", "addContact: Permissions Granted!");
            } else {
                Log.i("Debug", "addContact: Permission not yet granted!");
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, OverviewActivity.REQUEST_PERMISSIONS);
                return;
            }

            // access phone numbers
            Cursor phoneNumberCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{currentId}, null);
            while (phoneNumberCursor.moveToNext()) {
                currentNumber = phoneNumberCursor.getString(phoneNumberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                int currentNumberType = phoneNumberCursor.getInt(phoneNumberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA2));
                if (currentNumberType == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE) {
                    //this is mobile Number
                }
            }

            // access email address
            Cursor eMailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?", new String[]{currentId}, null);
            while (eMailCursor.moveToNext()) {
                currenteMailAddress = eMailCursor.getString(eMailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));

            }

            Contact newContact = new Contact(currentId, currentNumber, currentName, currenteMailAddress);


            item.addContact(newContact);
            updateContactListLayout();
            Toast.makeText(this, "created new ContactItem", Toast.LENGTH_SHORT).show();

        }
    }

    public void updateContactListLayout() {
        LinearLayout contactLayout = findViewById(R.id.contactsLayout);
        contactLayout.removeAllViews();


        for (final String i : item.getContacts()) {
            final Map<String, String> contactInformation = getContactInformation(i);
            String concatInformation = "";

            View view = getLayoutInflater().inflate(R.layout.activity_detailview_listitem, null);

            for (Map.Entry<String, String> entry: contactInformation.entrySet()){
                concatInformation = concatInformation + entry + "| ";
            }

            TextView contactName = view.findViewById(R.id.contact_item_name);
            contactName.setText(contactInformation.get("Name"));

            TextView contactPhone = view.findViewById(R.id.contact_item_phone);
            contactPhone.setText(contactInformation.get("Phone_Number"));
            Linkify.addLinks(contactPhone, Linkify.PHONE_NUMBERS);

            TextView contactMail = view.findViewById(R.id.contact_item_mail);
            contactMail.setText(contactInformation.get("Email"));
            Linkify.addLinks(contactMail, Linkify.EMAIL_ADDRESSES);

            ImageButton deleteContact = (ImageButton) view.findViewById(R.id.delete_contact_button);
            deleteContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.deleteContact(i);
                    //TODO dirty dirty dirty - but works
                    updateContactListLayout();
                }
            });

            final ImageButton sendSms = (ImageButton) view.findViewById(R.id.write_sms_contact_Button);
            if(contactInformation.get("Phone_Number") == null){
                sendSms.setEnabled(false);
                sendSms.setAlpha(.2f);
            }
            final String message = "Hey there! I have a Todo with you - " + "Name: " + item.getName() + " " + item.getDescription();
            sendSms.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    composeSMS(contactInformation.get("Phone_Number"), message);
                }
            });

            ImageButton sendEmail = (ImageButton) view.findViewById(R.id.write_email_contact_Button);
            if(contactInformation.get("Email") == null){
                sendEmail.setEnabled(false);
                sendEmail.setAlpha(.2f);
            }
            sendEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    composeEmail(DetailviewActivity.this, "New Todo for you! " + item.getName(), "This is your Todo " + item.getName() + " " + item.getDescription(), "Me", contactInformation.get("Email"));
                }
            });

            contactLayout.addView(view);

        }



    }

    public Map<String, String> getContactInformation(String id) {

        Map<String, String> contactInformation = new HashMap<String, String>();

        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
        while (cursor.moveToNext()) {
            contactInformation.put("Name", cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
            contactInformation.put("Phone_Number", cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));

        }
        cursor.close();

        Cursor eMailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?", new String[]{id}, null);
        while (eMailCursor.moveToNext()) {
            contactInformation.put("Email", eMailCursor.getString(eMailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)));
        }
        eMailCursor.close();

        
        Log.i("contacts", "this phone Numbers have bin fetched: " + contactInformation.toString());
        return contactInformation;
    }


    /**
     * compose an sms
     */
    // http://snipt.net/Martin/android-intent-usage/
    protected void composeSMS(String receiver, String message) {

        // the sms compose is identified by the tel: uri and the specified
        // action ACTION_SENDTO
        Uri smsUri = Uri.parse("smsto:" + receiver);
        Intent smsIntent = new Intent(Intent.ACTION_SENDTO, smsUri);
        smsIntent.putExtra("sms_body", message);

        startActivity(smsIntent);
    }


    // see
    // http://www.anddev.org/email_send_intent_intentchooser-t3295.html
    public static void composeEmail(Context context, String subject,
                                    String body, String sender, String recipients) {

        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        emailIntent.setType("plain/text");

        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                recipients.split(","));

        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);

        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

        // determine the activity to use for sending
        Intent chosenIntent = Intent.createChooser(emailIntent,
                "Sending Email...");


        context.startActivity(chosenIntent);

    }





}
