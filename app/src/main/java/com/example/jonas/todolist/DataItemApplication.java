package com.example.jonas.todolist;

import android.app.Application;
import android.os.AsyncTask;

import com.example.jonas.todolist.model.CrudMode;
import com.example.jonas.todolist.model.DataItem;
import com.example.jonas.todolist.model.IDataItemCRUDOperations;
import com.example.jonas.todolist.model.IDataItemCRUDOperationsAsync;
import com.example.jonas.todolist.model.LocalDataitemCRUDOperations;
import com.example.jonas.todolist.model.RemoteDataitemCRUDOperationsimpl;
import com.example.jonas.todolist.model.SimpleDataItemCRUDOperations;
import com.example.jonas.todolist.model.SyncedDataItemCRUDOperations;

import java.util.List;

public class DataItemApplication extends Application implements IDataItemCRUDOperationsAsync {

    private IDataItemCRUDOperations crudOperations;
    private LocalDataitemCRUDOperations offLineOperations;
    private SyncedDataItemCRUDOperations onLineOperations;

    private CrudMode crudMode = CrudMode.REMOTE;

    public static final String LOCAL_IP = "192.168.178.66";

    private Boolean connection;



    @Override
    public void onCreate() {
        super.onCreate();

        this.offLineOperations = new LocalDataitemCRUDOperations(this);
        //TODO replace Cast
        this.onLineOperations = new SyncedDataItemCRUDOperations(offLineOperations,new RemoteDataitemCRUDOperationsimpl());//new RemoteDataitemCRUDOperationsimpl();



    }


    public IDataItemCRUDOperationsAsync getCRUDOperations() {
        return this;
    }

    @Override
    public void createItem(DataItem item, final ResultCallback<Long> onresult) {
        new AsyncTask<DataItem, Void, Long>() {

            @Override
            protected Long doInBackground(DataItem... dataItems) {
                return crudOperations.createItem(dataItems[0]);
            }

            @Override
            protected void onPostExecute(Long id) {
                onresult.onresult(id);
            }
        }.execute(item);
    }

    @Override
    public void readAllItems(final ResultCallback<List<DataItem>> onresult) {
        new AsyncTask<Void, Void, List<DataItem>>() {
            @Override
            protected List<DataItem> doInBackground(Void... voids) {
                return crudOperations.readAllItems();
            }

            @Override
            protected void onPostExecute(List<DataItem> dataItems) {
                onresult.onresult(dataItems);
            }
        }.execute();

    }

    @Override
    public void readItem(long id, final ResultCallback<DataItem> onresult) {
        new AsyncTask<Long, Void, DataItem>() {
            @Override
            protected DataItem doInBackground(Long... longs) {
                return crudOperations.readItem(longs[0]);
            }

            @Override
            protected void onPostExecute(DataItem dataItem) {
                onresult.onresult(dataItem);
            }
        }.execute(id);

    }

    @Override
    public void updateItem(final long id, final DataItem item, final ResultCallback<Boolean> onresult) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                return crudOperations.updateItem(id, item);
            }

            @Override
            protected void onPostExecute(Boolean result) {
                onresult.onresult(result);
            }
        }.execute();
    }

    @Override
    public void deleteItem(final long id, final ResultCallback<Boolean> onresult) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                return crudOperations.deleteItem(id);
            }

            @Override
            protected void onPostExecute(Boolean result) {
                onresult.onresult(result);
            }
        }.execute();

    }

    public void setConnection(Boolean connection) {
        this.connection = connection;
    }


    public Boolean getConnection() {
        return connection;
    }


    public void setCrudMode(CrudMode crudMode) {
        this.crudMode = crudMode;
    }

    public CrudMode getCrudMode() {
        return crudMode;
    }

    public void startCrudOperation() {
        switch (crudMode) {
            case SIMPLE:
                this.crudOperations = new SimpleDataItemCRUDOperations();
                break;
            case LOCAL:
                this.crudOperations = offLineOperations;
                break;
            case REMOTE:
                this.crudOperations = onLineOperations;
                onLineOperations.doSync();

                break;
        }
    }



}
