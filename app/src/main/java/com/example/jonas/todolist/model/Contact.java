package com.example.jonas.todolist.model;

public class Contact {

    private String id;
    private String phoneNumber;
    private String name;
    private String eMail;

    public Contact(String id, String phoneNumber, String name, String eMail) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.eMail = eMail;
    }

    public String getId() {
        return id;
    }
}
