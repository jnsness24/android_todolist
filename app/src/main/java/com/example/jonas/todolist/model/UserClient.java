package com.example.jonas.todolist.model;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;

public interface UserClient {
    @PUT("users/auth")
    public Call<Boolean> authenticateUser(@Body User user);
}