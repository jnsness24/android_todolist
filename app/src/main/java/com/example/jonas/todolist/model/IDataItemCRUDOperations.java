package com.example.jonas.todolist.model;

import java.util.List;

public interface IDataItemCRUDOperations {

    public long createItem(DataItem item);

    public List<DataItem> readAllItems();

    public DataItem readItem(long id);

    public boolean updateItem(long id, DataItem item);

    public boolean deleteItem(long id);
}
