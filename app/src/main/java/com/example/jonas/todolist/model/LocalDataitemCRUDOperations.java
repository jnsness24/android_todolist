package com.example.jonas.todolist.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class LocalDataitemCRUDOperations implements IDataItemCRUDOperations {

    private SQLiteDatabase db;

    private static final String TABLE_DATAITEMS = "DATAITEMS";

    private static final String[] ALL_COLUMNS = new String[]{"ID", "NAME","DESCRIPTION", "EXPIRY", "DONE", "FAVOURITE", "CONTACTS"};

    private static final String CREATION_QUERY = "CREATE TABLE DATAITEMS (ID INTEGER PRIMARY KEY,NAME TEXT, DESCRIPTION TEXT, EXPIRY INTEGER, DONE INTEGER, FAVOURITE INTEGER, CONTACTS STRING)";
    private static final String DEBUG_DELETE = "DROP TABLE DATAITEMS";

    public LocalDataitemCRUDOperations(Context ctx) {
        this.db = ctx.openOrCreateDatabase("mysqlitedb.sqlite", Context.MODE_PRIVATE, null);

//        db.execSQL(DEBUG_DELETE);
//        db.setVersion(0);


        if (db.getVersion() == 0) {
            db.setVersion(1);
            db.execSQL(CREATION_QUERY);
        }
    }

    @Override
    public long createItem(DataItem item) {

        ContentValues values = new ContentValues();
        values.put("NAME", item.getName());
        values.put("DESCRIPTION", item.getDescription());
        values.put("EXPIRY", item.getExpiry());
        values.put("DONE", item.isDone() ? 1 : 0);
        values.put("FAVOURITE", item.isFavourite() ? 1 : 0);

        //String convertedContactArray = convertArrayToString(item.getContacts());
//        values.put("CONTACTS", convertedContactArray);
        ArrayList<String> contactDebug = item.getContacts();
        if (contactDebug == null) {
            contactDebug = new ArrayList<String>();
        }

        String convertedContactArray = convertArrayToString(contactDebug);
        values.put("CONTACTS", convertedContactArray);

        long id = db.insert(TABLE_DATAITEMS, null, values);
        item.setId(id);

        return id;
    }

    @Override
    public List<DataItem> readAllItems() {

        List<DataItem> items = new ArrayList<>();

        Cursor cursor = db.query(TABLE_DATAITEMS, ALL_COLUMNS, null, null, null, null, "DONE");
        String[] columnNames = cursor.getColumnNames();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                items.add(createDataItemFromCursor(cursor));
            }
        }


        return items;
    }

    public DataItem createDataItemFromCursor(Cursor cursor) {
        DataItem item = new DataItem();
        item.setId(cursor.getLong(cursor.getColumnIndex("ID")));
        item.setName(cursor.getString(cursor.getColumnIndex("NAME")));
        item.setDescription(cursor.getString(cursor.getColumnIndex("DESCRIPTION")));
        item.setExpiry(cursor.getLong(cursor.getColumnIndex("EXPIRY")));
        item.setFavourite(cursor.getInt(cursor.getColumnIndex("FAVOURITE")) == 1);
        item.setDone(cursor.getInt(cursor.getColumnIndex("DONE")) == 1);

        String dbContactResult = (cursor.getString(cursor.getColumnIndex("CONTACTS")));
        String[] contactArray = convertStringToArray(dbContactResult);

        // this stringarray helps with sqlite, that has no array type
        ArrayList<String> newContactList = new ArrayList<String>();
        for(int i=0; i<contactArray.length;i++) {
            newContactList.add(contactArray[i]);
        }

        // this fixes bug, that an item that has no contacts, got an empty string as first contact. Has to do with sqlite Datatype hazzle
        if (newContactList.size() == 1 && newContactList.get(0).isEmpty()) {
            newContactList.clear();
        }

//        ArrayList<String> newContactList = (ArrayList<String>) Arrays.asList(contactArray);
        item.setContacts(newContactList);

        return item;

    }

    @Override
    public DataItem readItem(long id) {
        Cursor cursor = db.query(TABLE_DATAITEMS, ALL_COLUMNS, "ID=?", new String[]{String.valueOf(id)}, null, null, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            return createDataItemFromCursor(cursor);
        }

        return null;
    }

    @Override
    public boolean updateItem(long id, DataItem item) {
        ContentValues newValues = new ContentValues();
        newValues.put("NAME", item.getName());
        newValues.put("DESCRIPTION", item.getDescription());
        newValues.put("EXPIRY", item.getExpiry());
        newValues.put("DONE", item.isDone() ? 1 : 0);
        newValues.put("FAVOURITE", item.isFavourite() ? 1 : 0);

        String convertedContactArray = convertArrayToString(item.getContacts());
        newValues.put("CONTACTS", convertedContactArray);


        db.update(TABLE_DATAITEMS, newValues, "id=" + id, null);
        return true;
    }

    @Override
    public boolean deleteItem(long id) {
        db.delete(TABLE_DATAITEMS, "id=" + id, null);
        return true;
    }

    public static String convertArrayToString(ArrayList<String> array){
        String str = "";

        for (int i = 0; i<array.size(); i++) {
            str = str + array.get(i);
            if (i < array.size() - 1) {
                str = str + "__,__";
            }
        }
        return str;
    }

    public static String[] convertStringToArray(String string) {
        String[] arr = string.split("__,__");
        return arr;
    }
}
