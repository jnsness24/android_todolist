package com.example.jonas.todolist.model;

import java.util.ArrayList;
import java.util.List;

public class SimpleDataItemCRUDOperations implements IDataItemCRUDOperations {

    private static final DataItem[] mockitems = new DataItem[]{
            new DataItem("test"),
            new DataItem("another test"),
            new DataItem("the last static item")
    };

    private List<DataItem> items = new ArrayList<>();
    private long idcount = 0;

    public SimpleDataItemCRUDOperations() {
        for (DataItem item : mockitems) {
            createItem(item);
        }
    }

    @Override
    public long createItem(DataItem item) {
        item.setId(++idcount);
        items.add(item);

        return item.getId();
    }


    @Override
    public List<DataItem> readAllItems() {
        return items;
    }

    @Override
    public DataItem readItem(long id) {
        for (DataItem item : items) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }

    @Override
    public boolean updateItem(long id, DataItem item) {
        for (DataItem obj : items) {
            if (obj.getId() == id) {
                items.set(items.indexOf(obj), item);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean deleteItem(long id) {
        for (DataItem obj : items) {
            if (obj.getId() == id) {
                items.remove(obj);
                return true;
            }
        }
        return false;
    }
}
