package com.example.jonas.todolist.model;

public class User {

    private String pwd;
    private String email;


    public User(String email, String pwd) {
        this.email = email;
        this.pwd = pwd;
    }

    public String getPwd() {
        return pwd;
    }

    public String getEmail() {
        return email;
    }
}
