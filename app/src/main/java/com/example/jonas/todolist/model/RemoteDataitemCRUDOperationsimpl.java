package com.example.jonas.todolist.model;

import android.util.Log;

import com.example.jonas.todolist.DataItemApplication;

import java.io.IOException;
import java.util.List;

//import lombok.Getter;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public class RemoteDataitemCRUDOperationsimpl implements IDataItemCRUDOperations {

    public static interface TodoWebAPI {

        @POST("todos")
        public Call<DataItem> createItem(@Body DataItem item);

        @GET("todos")
        public Call<List<DataItem>> readAllDataitems();

        @GET("todos/{id}")
        public Call<DataItem> readDataItem(@Path("id") long id);

        @PUT("todos/{id}")
        public Call<DataItem> updateDataItem(@Path("id") long id, @Body DataItem item);

        @DELETE("todos/{id}")
        public Call<Boolean> deleteItem(@Path("id") long id);

        @DELETE("todos")
        public Call<Boolean> deleteAll();

    }

    private TodoWebAPI serviceProxy;

    public RemoteDataitemCRUDOperationsimpl() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + DataItemApplication.LOCAL_IP + ":8080/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //magic function retrofit.create - dynamic proxy
        serviceProxy = retrofit.create(TodoWebAPI.class);
    }

    @Override
    public long createItem(DataItem item) {
        try {
            //TODO write in one line - when debugging is finished. For Debugger this variables are more readable
            Call<DataItem> call = serviceProxy.createItem(item);
            Response<DataItem> response = call.execute();
            DataItem callBody = response.body();
            return callBody.getId();
        } catch (IOException e) {
            Log.i("Debug", "createItem: Remote Creation failed");
            throw new RuntimeException(e);
        }

    }


    @Override
    public List<DataItem> readAllItems() {
        try {
            return serviceProxy.readAllDataitems().execute().body();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public DataItem readItem(long id) {
        try {
            return serviceProxy.readDataItem(id).execute().body();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean updateItem(long id, DataItem item) {
        try {
            return serviceProxy.updateDataItem(id, item).execute().body() != null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean deleteItem(long id) {
        try {
            return serviceProxy.deleteItem(id).execute().body() != null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean deleteAll() {
        try {
            return serviceProxy.deleteAll().execute().body() != null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}


