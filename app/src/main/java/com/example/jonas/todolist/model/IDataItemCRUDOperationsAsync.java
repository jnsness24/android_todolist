package com.example.jonas.todolist.model;

import android.service.carrier.CarrierMessagingService;

import java.util.List;

public interface IDataItemCRUDOperationsAsync {

    public static interface ResultCallback<T> {
        public void onresult(T result);
    }


    public void createItem(DataItem item, ResultCallback<Long> onresult);

    public void readAllItems(ResultCallback<List<DataItem>> onresult);

    public void readItem(long id, ResultCallback<DataItem> onresult);

    public void updateItem(long id, DataItem item,ResultCallback<Boolean> onresult);

    public void deleteItem(long id, ResultCallback<Boolean> onresult);

}
